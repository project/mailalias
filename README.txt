Description
------

With this module, a user may associate additional email addresses with his account. These addresses can be used by mailhandler.module to authenticate incoming emails.

Usage
------------

Users will now see a textfield on their 'edit account' page called Email Aliases.

Author
------

Moshe Weitzman <weitzman@tejasa.com>. A new maintainer is wanted.


